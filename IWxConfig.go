package gowe

//IWxConfig 微信的基础配置
type IWxConfig interface {
	GetId() string          //Id 数据库记录的Id
	GetAppId() string       //AppId 微信号的appId
	GetAccessToken() string //AccessToken 获取到AccessToken
	GetSecret() string      //Secret 微信号的secret
}

//IWxMpConfig 公众号的配置
type IWxMpConfig interface {
	IWxConfig
	GetToken() string  //Token 获取token
	GetAesKey() string //AesKey 获取aesKey
	GetOauth2() bool   //开启oauth2.0认证,是否能够获取openId,0是关闭,1是开启
}

//IWxMaConfig 微信小程序配置
type IWxMaConfig interface {
	IWxConfig
}

//IWxPayConfig 公众号的配置
type IWxPayConfig interface {
	IWxConfig
	GetCertificateFile() string //证数文件路径
	GetMchId() string           //支付的mchId
	GetSubAppId() string        // 微信分配的子商户公众账号ID
	GetSubMchId() string        // 微信支付分配的子商户号,开发者模式下必填
	GetAPIKey() string          //获取 API 密钥
	GetNotifyUrl() string       //支付通知回调的地址
	GetSignType() string        //摘要加密类型
	GetServiceType() int        // 服务模式
	IsProd() bool               // 是否是生产环境
}
