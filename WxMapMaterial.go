package gowe

import (
	"encoding/json"
	"fmt"
	"os"
)

type WxMapMaterialMedia struct {
	Type      string `json:"type"`
	MediaId   string `json:"media_id"`
	CreatedAt int    `json:"created_at"`
}

func WxMpMaterialupLoadMedia(wxMpConfig IWxMpConfig, mediaType, media string, file *os.File) (*WxMapMaterialMedia, *error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/media/upload?access_token=" + accessToken + "&type=" + mediaType + "&media=1111"
	fmt.Println(url)
	response, err := httpPostFile(url, file)
	if err != nil {
		return nil, &err
	}
	fmt.Println(string(response))
	result := &WxMapMaterialMedia{}
	err = json.Unmarshal(response, result)
	return result, nil
}
