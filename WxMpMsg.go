package gowe

import (
	"encoding/xml"
	"time"
)

type MsgBuilder struct {
}

//消息的基础组成结构
type WeChatMpBaseMsg struct {
	ToUserName   string `json:"ToUserName"`
	FromUserName string `json:"FromUserName"`
	CreateTime   string `json:"CreateTime"`
	MsgType      string `json:"MsgType"`
	MsgId        string `json:"MsgId"`
}

//文本消息
type WxMpTextMsg struct {
	WeChatMpBaseMsg
	Content string `json:"Content"`
}

//图片消息
type WxMpPicMsg struct {
	WeChatMpBaseMsg
	PicUrl  string `json:"PicUrl"`
	MediaId string `json:"MediaId"`
}

//语音消息
type WxMpVoiceMsg struct {
	WeChatMpBaseMsg
	Format      string `json:"Format"`
	MediaId     string `json:"MediaId"`
	Recognition string `json:"Recognition"`
}

//视频消息
type WxMpVideoMsg struct {
	WeChatMpBaseMsg
	Format       string `json:"Format"`
	MediaId      string `json:"MediaId"`
	ThumbMediaId string `json:"ThumbMediaId"`
}

//小视频消息
type WxMpShortVideoMsg struct {
	WxMpVideoMsg
}

//地理位置消息
type WxMpLocationMsg struct {
	WeChatMpBaseMsg
	LocationX string `json:"Location_X"`
	LocationY string `json:"Location_Y"`
	Scale     string `json:"Scale"`
	Label     string `json:"Label"`
}

//链接消息
type WxMpLinkMsg struct {
	WeChatMpBaseMsg
	Title       string `json:"Title"`
	Description string `json:"Description"`
	Url         string `json:"Url"`
}

//扫描带参数二维码事件
type WxMpEventQrscene struct {
	WeChatMpBaseMsg
	Event    string `json:"Event"`
	EventKey string `json:"EventKey"`
}

//用户已关注时的事件推送
type WxMpEventScan struct {
	WxMpEventQrscene
}

//上报地理位置事件
type WxMpEventLocation struct {
	Event     string `json:"Event"`
	Latitude  string `json:"Latitude"`
	Longitude string `json:"Longitude"`
	Precision string `json:"Precision"`
}

//自定义菜单事件
type WxMpEventClick struct {
	WxMpEventQrscene
}

//点击菜单跳转链接时的事件推送
type WxMpEventView struct {
	WxMpEventQrscene
}

//回复文本消息
type WxMpReplyTextMsg struct {
	XMLName      xml.Name `xml:"xml"` // 指定最外层的标签为 xml
	ToUserName   string   `json:"ToUserName"`
	FromUserName string   `json:"FromUserName"`
	CreateTime   string   `json:"CreateTime"`
	MsgType      string   `json:"MsgType"`
	Content      string   `json:"Content"`
}

//创建文本回复消息
func (msgBuilder *MsgBuilder) CreateReplyTextMsg(FromUserName string, ToUserName string, Content string) WxMpReplyTextMsg {
	chatMpReplyTextMsg := WxMpReplyTextMsg{}
	chatMpReplyTextMsg.MsgType = "text"
	chatMpReplyTextMsg.CreateTime = time.Now().String()
	chatMpReplyTextMsg.FromUserName = FromUserName
	chatMpReplyTextMsg.ToUserName = ToUserName
	chatMpReplyTextMsg.Content = Content
	return chatMpReplyTextMsg
}

//回复图片消息
type WxMpReplyImageMsg struct {
	XMLName      xml.Name `xml:"xml"` // 指定最外层的标签为 xml
	ToUserName   string   `json:"ToUserName"`
	FromUserName string   `json:"FromUserName"`
	CreateTime   string   `json:"CreateTime"`
	MsgType      string   `json:"MsgType"`
	Image        struct {
		MediaId string `json:"MediaId"`
	} `json:"Image"`
}

//创建文图片复消息
func (msgBuilder *MsgBuilder) CreateReplyImageMsg(FromUserName string, ToUserName string, MediaId string) WxMpReplyImageMsg {
	weChatMpReplyImageMsg := WxMpReplyImageMsg{}
	weChatMpReplyImageMsg.FromUserName = FromUserName
	weChatMpReplyImageMsg.ToUserName = ToUserName
	weChatMpReplyImageMsg.MsgType = "image"
	weChatMpReplyImageMsg.CreateTime = time.Now().String()
	weChatMpReplyImageMsg.Image.MediaId = MediaId
	return weChatMpReplyImageMsg
}

//回复语音消息
type WxMpReplyVoiceMsg struct {
	XMLName      xml.Name `xml:"xml"` // 指定最外层的标签为 xml
	ToUserName   string   `json:"ToUserName"`
	FromUserName string   `json:"FromUserName"`
	CreateTime   string   `json:"CreateTime"`
	MsgType      string   `json:"MsgType"`
	Voice        struct {
		MediaId string `json:"MediaId"`
	} `json:"Voice"`
}

//创建回复语音消息
func (msgBuilder *MsgBuilder) CreateReplyVoiceMsg(FromUserName string, ToUserName string, MediaId string) WxMpReplyVoiceMsg {
	weChatMpReplyVoiceMsg := WxMpReplyVoiceMsg{}
	weChatMpReplyVoiceMsg.FromUserName = FromUserName
	weChatMpReplyVoiceMsg.ToUserName = ToUserName
	weChatMpReplyVoiceMsg.MsgType = "voice"
	weChatMpReplyVoiceMsg.CreateTime = time.Now().String()
	weChatMpReplyVoiceMsg.Voice.MediaId = MediaId
	return weChatMpReplyVoiceMsg
}

//回复视频消息
type WxMpReplyVideoMsg struct {
	XMLName      xml.Name `xml:"xml"` // 指定最外层的标签为 xml
	ToUserName   string   `json:"ToUserName"`
	FromUserName string   `json:"FromUserName"`
	CreateTime   string   `json:"CreateTime"`
	MsgType      string   `json:"MsgType"`
	Video        struct {
		MediaId     string `json:"MediaId"`
		Title       string `json:"Title"`
		Description string `json:"Description"`
	} `json:"Video"`
}

//创建回复视频消息
func (msgBuilder *MsgBuilder) CreateReplyVideoMsg(FromUserName string, ToUserName string, MediaId string, Title string, Description string) WxMpReplyVideoMsg {
	weChatMpReplyVideoMsg := WxMpReplyVideoMsg{}
	weChatMpReplyVideoMsg.FromUserName = FromUserName
	weChatMpReplyVideoMsg.ToUserName = ToUserName
	weChatMpReplyVideoMsg.MsgType = "video"
	weChatMpReplyVideoMsg.CreateTime = time.Now().String()
	weChatMpReplyVideoMsg.Video.Title = Title
	weChatMpReplyVideoMsg.Video.MediaId = MediaId
	weChatMpReplyVideoMsg.Video.Description = Description
	return weChatMpReplyVideoMsg
}

//回复音乐消息
type WxMpReplyMusicMsg struct {
	XMLName      xml.Name `xml:"xml"` // 指定最外层的标签为 xml
	ToUserName   string   `json:"ToUserName"`
	FromUserName string   `json:"FromUserName"`
	CreateTime   string   `json:"CreateTime"`
	MsgType      string   `json:"MsgType"`
	Music        struct {
		Title        string `json:"Title"`
		Description  string `json:"Description"`
		MusicUrl     string `json:"MusicUrl"`
		HQMusicUrl   string `json:"HQMusicUrl"`
		ThumbMediaId string `json:"ThumbMediaId"`
	} `json:"Music"`
}

//创建回复音乐消息
func (msgBuilder *MsgBuilder) CreateReplyMusicMsg(FromUserName string,
	ToUserName string,
	MusicUrl string,
	HQMusicUrl string,
	Title string,
	Description string) WxMpReplyMusicMsg {
	weChatMpReplyMusicMsg := WxMpReplyMusicMsg{}
	weChatMpReplyMusicMsg.FromUserName = FromUserName
	weChatMpReplyMusicMsg.ToUserName = ToUserName
	weChatMpReplyMusicMsg.MsgType = "music"
	weChatMpReplyMusicMsg.CreateTime = time.Now().String()
	weChatMpReplyMusicMsg.Music.Title = Title
	weChatMpReplyMusicMsg.Music.MusicUrl = MusicUrl
	weChatMpReplyMusicMsg.Music.HQMusicUrl = HQMusicUrl
	weChatMpReplyMusicMsg.Music.Description = Description
	return weChatMpReplyMusicMsg
}

//回复图文消息
type WxMpReplyArticlesMsg struct {
	XMLName      xml.Name `xml:"xml"` // 指定最外层的标签为 xml
	ToUserName   string   `json:"ToUserName"`
	FromUserName string   `json:"FromUserName"`
	CreateTime   string   `json:"CreateTime"`
	MsgType      string   `json:"MsgType"`
	ArticleCount int      `json:"ArticleCount"`
	Articles     struct {
		Item struct {
			Title       string `json:"Title"`
			Description string `json:"Description"`
			PicUrl      string `json:"PicUrl"`
			Url         string `json:"Url"`
		} `json:"item"`
	} `json:"Articles"`
}

//创建回复文章消息
func (msgBuilder *MsgBuilder) CreateReplyArticlesMsg(FromUserName string,
	ToUserName string,
	Url string,
	PicUrl string,
	Title string,
	Description string,
	ArticleCount int) WxMpReplyArticlesMsg {
	weChatMpReplyArticlesMsg := WxMpReplyArticlesMsg{}
	weChatMpReplyArticlesMsg.FromUserName = FromUserName
	weChatMpReplyArticlesMsg.ToUserName = ToUserName
	weChatMpReplyArticlesMsg.MsgType = "news"
	weChatMpReplyArticlesMsg.CreateTime = time.Now().String()
	weChatMpReplyArticlesMsg.ArticleCount = ArticleCount
	weChatMpReplyArticlesMsg.Articles.Item.Url = Url
	weChatMpReplyArticlesMsg.Articles.Item.PicUrl = PicUrl
	weChatMpReplyArticlesMsg.Articles.Item.Title = Title
	weChatMpReplyArticlesMsg.Articles.Item.Description = Description
	return weChatMpReplyArticlesMsg
}
