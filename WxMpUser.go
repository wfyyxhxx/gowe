package gowe

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

//创建用户标签
func WxMpUserTagsCreate(wxMpConfig IWxMpConfig, name string) (wxMpUserTag *WxMpUserTag, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	fmt.Println("accessToken=" + accessToken)
	apiurl := WxMpAPIURL + "/cgi-bin/tags/create?access_token=" + accessToken
	params := map[string](map[string]string){}
	params["tag"] = map[string]string{"name": name}
	jsonParam, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	response, err := http.Post(apiurl, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	resp := WxMpUserTag{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &resp)
	return &resp, nil
}

//创建用户标签的接口结果model
type WxMpUserTag struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

//创建用户标签的接口结果model
type WxMpUserTagCreateResp struct {
	Tag WxMpUserTag `json:"tag"`
}

//获取用户标签
func WxMpUserTagsGet(wxMpConfig IWxMpConfig) (wxMpUserTags *WxMpUserTags, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/get?access_token=" + accessToken
	fmt.Println(url)
	data, err := httpGet(url)
	if err != nil {
		return nil, err
	}
	resp := WxMpUserTags{}
	err = json.Unmarshal(data, &resp)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

type WxMpUserTags struct {
	Tags []struct {
		WxMpUserTag
		Count int `json:"count"`
	} `json:"tags"`
}

//WxAccessToken 微信accessToken
type WxMpError struct {
	ErrCode int    `json:"errcode"` // 错误码
	ErrMsg  string `json:"errmsg"`  // 错误信息
}

func (this *WxMpError) isOk() bool {
	return this.ErrCode == 0
}

//获取用户标签
func WxMpUserTagsUpdate(wxMpConfig IWxMpConfig, wxMpUserTag *WxMpUserTag) (wxMpError *WxMpError, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/update?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]WxMpUserTag{"tag": *wxMpUserTag}
	jsonParam, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	response, err := http.Post(url, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	mpError := WxMpError{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &mpError)
	return &mpError, nil
}

//删除用户标签
func WxMpUserTagsDatele(wxMpConfig IWxMpConfig, id int) (wxMpError *WxMpError, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/delete?access_token=" + accessToken
	fmt.Println(url)
	param := map[string](map[string]int){"tag": {"id": id}}
	jsonParam, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	response, err := http.Post(url, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	mpError := WxMpError{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &mpError)
	return &mpError, nil
}

type WxMpUserTagsFansList struct {
	Count int `json:"count"`
	Data  struct {
		Openid []string `json:"openid"`
	} `json:"data"`
	NextOpenid string `json:"next_openid"`
}

//获取标签下粉丝列表
func WxMpUserTagsGetFans(wxMpConfig IWxMpConfig, tagid int, next_openid string) (wxMpUserTagsFansList *WxMpUserTagsFansList, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/user/tag/get?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{}
	param["tagid"] = tagid
	if next_openid == "" {
		param["next_openid"] = nil
	}
	jsonParam, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	response, err := http.Post(url, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	fansList := WxMpUserTagsFansList{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &fansList)
	return &fansList, nil
}

//批量为用户打标签
func WxMpUserTagsBatchSetTags(wxMpConfig IWxMpConfig, tagid int, openid_list ...string) (wxMpError *WxMpError, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/members/batchtagging?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{}
	param["tagid"] = tagid
	param["openid_list"] = openid_list
	jsonParam, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	fmt.Println(string(jsonParam))
	response, err := http.Post(url, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	result := WxMpError{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &result)
	return &result, nil
}

//批量为用户取消标签
func WxMpUserTagsBatchUnsetTags(wxMpConfig IWxMpConfig, tagid int, openid_list ...string) (wxMpError *WxMpError, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/members/batchuntagging?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{}
	param["tagid"] = tagid
	param["openid_list"] = openid_list
	jsonParam, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	fmt.Println(string(jsonParam))
	response, err := http.Post(url, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	result := WxMpError{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &result)
	return &result, nil
}

type WxMpUserTagidList struct {
	TagidList []int `json:"tagid_list"`
}

//获取用户身上的标签列表
func WxMpUserTagsGetTags(wxMpConfig IWxMpConfig, openid string) (wxMpUserTagidList *WxMpUserTagidList, err error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/getidlist?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{}
	param["openid"] = openid
	jsonParam, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	fmt.Println(string(jsonParam))
	response, err := http.Post(url, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	result := WxMpUserTagidList{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &result)
	return &result, nil
}

//设置用户备注名
func WxMpUserSetRemark(wxMpConfig IWxMpConfig, openid string, remark string) (*WxMpError, error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/user/info/updateremark?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{"openid": openid, "remark": remark}
	jsonParam, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	fmt.Println(string(jsonParam))
	response, err := http.Post(url, "application/json; charset=utf-8", bytes.NewReader(jsonParam))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	respBody, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		return nil, errRead
	}
	result := WxMpError{}
	fmt.Println(string(respBody))
	err = json.Unmarshal(respBody, &result)
	return &result, nil
}

//获取用户基本信息(UnionID机制)
func WxMpUserGetInfo(wxMpConfig IWxMpConfig, openId string, lang string) (*WxMpUserInfo, error) {
	return WxMpGetUserInfo(wxMpConfig, openId, lang)
}

type WxMpUserList struct {
	Total int `json:"total"`
	Count int `json:"count"`
	Data  struct {
		Openid []string `json:"openid"`
	} `json:"data"`
	NextOpenid string `json:"next_openid"`
}

//获取用户列表
func WxMpUserGetList(wxMpConfig IWxMpConfig, next_openid string) (*WxMpUserList, error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/user/get?access_token=" + accessToken
	if url != "" {
		url += "&next_openid" + next_openid
	}
	fmt.Println(url)
	data, err := httpGet(url)
	if err != nil {
		return nil, err
	}
	resp := WxMpUserList{}
	err = json.Unmarshal(data, &resp)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

type WxMpUserBlackList struct {
	Total int `json:"total"`
	Count int `json:"count"`
	Data  struct {
		Openid []string `json:"openid"`
	} `json:"data"`
	NextOpenid string `json:"next_openid"`
}

//获取公众号的黑名单列表
func WxMpUserGetBlackList(wxMpConfig IWxMpConfig, begin_openid string) (*WxMpUserBlackList, error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/members/getblacklist?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{} //{"begin_openid":begin_openid}
	if begin_openid != "" {
		param["begin_openid"] = begin_openid
	}
	response, err := httpPost(url, param)
	if err != nil {
		return nil, err
	}
	result := WxMpUserBlackList{}
	fmt.Println(string(response))
	err = json.Unmarshal(response, &result)
	return &result, nil
}

//批量拉黑用户
func WxMpUserSetBatchBlack(wxMpConfig IWxMpConfig, openid_list ...string) (*WxMpError, error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/members/batchblacklist?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{"openid_list": openid_list}
	response, err := httpPost(url, param)
	if err != nil {
		return nil, err
	}
	result := WxMpError{}
	fmt.Println(string(response))
	err = json.Unmarshal(response, &result)
	return &result, nil
}

//取消拉黑用户
func WxMpUserUnsetBatchBlack(wxMpConfig IWxMpConfig, openid_list ...string) (*WxMpError, error) {
	accessToken := wxMpConfig.GetAccessToken()
	url := WxMpAPIURL + "/cgi-bin/tags/members/batchunblacklist?access_token=" + accessToken
	fmt.Println(url)
	param := map[string]interface{}{"openid_list": openid_list}
	response, err := httpPost(url, param)
	if err != nil {
		return nil, err
	}
	result := &WxMpError{}
	fmt.Println(string(response))
	err = json.Unmarshal(response, result)
	return result, nil
}
