package gowe

import (
	"fmt"
	"os"
	"testing"
)

type WxConfig struct {
	Id     string
	AppId  string
	Secret string
}

var wxConfig = &WxConfig{
	Id:     "gh_c4ae8d748ac7",
	AppId:  "wx9080501e21943d16",
	Secret: "3b2e7b83677db96ed5cd7f86d6434a69",
}

func (wxConfig *WxConfig) GetId() string    { return wxConfig.Id }
func (wxConfig *WxConfig) GetAppId() string { return wxConfig.AppId }
func (wxConfig *WxConfig) GetAccessToken() string {
	//从缓存中获取wxAccessToken,这里只是演示
	wxAccessToken, err := GetAccessToken(wxConfig)
	if err == nil && wxAccessToken.ErrCode == 0 {
		return wxAccessToken.AccessToken
	}
	return ""
	//return "https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=48_-Iltv-tgbIru5AS7L8POFHPOkZxW0l-NgfY3y6vPC-omnsODTZRz2kbaiIHttkqZaS-ColJgw_azGxaBeWtkpLPnnaMXZv_oNCS5IXduEztoa7wwL7_OAk3WiBw98zEyT8MH_csjwWTI-wViYJDbAJAUFZhttps://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=48_-Iltv-tgbIru5AS7L8POFHPOkZxW0l-NgfY3y6vPC-omnsODTZRz2kbaiIHttkqZaS-ColJgw_azGxaBeWtkpLPnnaMXZv_oNCS5IXduEztoa7wwL7_OAk3WiBw98zEyT8MH_csjwWTI-wViYJDbAJAUFZ"
}
func (wxConfig *WxConfig) GetSecret() string { return wxConfig.Secret }
func (wxConfig *WxConfig) GetToken() string  { return wxConfig.GetAccessToken() } //Token 获取token
func (wxConfig *WxConfig) GetAesKey() string { return "" }                        //AesKey 获取aesKey
func (wxConfig *WxConfig) GetOauth2() bool   { return false }                     //开启oauth2.0认证,是否能够获取openId,0是关闭,1是开启

//测试创建用户标签
func Test_WxMpUserTagsCreate(t *testing.T) {
	userTags, err := WxMpUserTagsCreate(wxConfig, "7")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(userTags.Name)
	fmt.Println(userTags.Id)
}

//测试创建用户标签
func Test_WxMpUserTagsGet(t *testing.T) {
	userTags, err := WxMpUserTagsGet(wxConfig)
	if err != nil {
		fmt.Println(err)
	} else {
		for i := 0; i < len(userTags.Tags); i++ {
			fmt.Println(userTags.Tags[i].Id)
			fmt.Println(userTags.Tags[i].Name)
			fmt.Println(userTags.Tags[i].Count)
		}
	}
}

//测试创建用户标签
func Test_WxMpUserTagsUpdate(t *testing.T) {
	wxMpUserTag := WxMpUserTag{
		Id:   100,
		Name: "星标组1",
	}
	wxMpError, err := WxMpUserTagsUpdate(wxConfig, &wxMpUserTag)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(wxMpError.isOk())
}

//测试创建用户标签
func Test_WxMpUserTagsDetele(t *testing.T) {
	wxMpError, err := WxMpUserTagsDatele(wxConfig, 101)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(wxMpError.isOk())
}

//获取标签下粉丝列表
func Test_WxMpUserTagsGetFans(t *testing.T) {
	wxMpUserTagsFansList, err := WxMpUserTagsGetFans(wxConfig, 102, "")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(wxMpUserTagsFansList.Count)
}

//批量为用户打标签
func Test_WxMpUserTagsBatchSetTags(t *testing.T) {
	result, err := WxMpUserTagsBatchSetTags(wxConfig, 102, "or-wB56lh9F-41eOSTZm1y-9o8-8")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result.isOk())
}

//批量为用户取消标签
func Test_WxMpUserTagsBatchUnsetTags(t *testing.T) {
	result, err := WxMpUserTagsBatchUnsetTags(wxConfig, 102, "or-wB56lh9F-41eOSTZm1y-9o8-8")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result.isOk())
}

//获取用户身上的标签列表
func Test_WxMpUserTagsGetTags(t *testing.T) {
	result, err := WxMpUserTagsGetTags(wxConfig, "or-wB56lh9F-41eOSTZm1y-9o8-8")
	if err != nil {
		fmt.Println(err)
	}
	for i := range result.TagidList {
		fmt.Println(result.TagidList[i])
	}
}

//设置用户备注名
func Test_WxMpUserSetRemark(t *testing.T) {
	result, err := WxMpUserSetRemark(wxConfig, "or-wB56lh9F-41eOSTZm1y-9o8-8", "wangfan")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result.isOk())
}

//获取用户列表
func Test_WxMpUserGetList(t *testing.T) {
	result, err := WxMpUserGetList(wxConfig, "")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(*result)
}

//获取公众号的黑名单列表
func Test_WxMpUserGetBlackList(t *testing.T) {
	result, err := WxMpUserGetBlackList(wxConfig, "")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(*result)
}

//批量拉黑用户
func Test_WxMpUserSetBatchBlack(t *testing.T) {
	result, err := WxMpUserSetBatchBlack(wxConfig, "or-wB56lh9F-41eOSTZm1y-9o8-8")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result.isOk())
}

//批量取消拉黑用户
func Test_WxMpUserUnsetBatchBlack(t *testing.T) {
	result, err := WxMpUserUnsetBatchBlack(wxConfig, "or-wB56lh9F-41eOSTZm1y-9o8-8")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result.isOk())
}

func Test_WxMpMaterialupLoadMedia(t *testing.T) {
	file, _ := os.Open("/home/wangfan/Desktop/20210822215124.png")
	media, _ := WxMpMaterialupLoadMedia(wxConfig, "image", "", file)
	fmt.Println(*media)
}
